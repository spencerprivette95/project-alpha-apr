from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from projects.models import Project
from tasks.models import Task
from projects.forms import CreateProjectForm


@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list_projects,
    }
    return render(request, "projects/home.html", context)


@login_required
def show_project(request, id):
    show_project = get_object_or_404(
        Project,
        id=id,
    )
    tasks = Task.objects.filter(project=show_project)

    context = {
        "project": show_project,
        "tasks": tasks,
    }
    # print(show_project.task.all())
    # print(show_project)
    # print(tasks)
    # print(tasks.project_id)
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
